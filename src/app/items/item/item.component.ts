import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { switchMap } from 'rxjs/operators';
import { ItemModel } from 'src/app/models/misc/api/Item.model';
import { CartService } from 'src/app/services/cart.service';
import { ItemsService } from 'src/app/services/items.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.sass']
})
export class ItemComponent implements OnInit {

  public itemId!: string;
  public item!: ItemModel | undefined;
  constructor(
    private itemService: ItemsService, 
    private route: ActivatedRoute,
    private cartService: CartService,
    private toasterService: ToastrService) { }

  ngOnInit() {
    this.route.paramMap.pipe(switchMap(p => {
      this.itemId = p.get("itemId") || '';
      return this.itemService.getItemById(this.itemId);
    })).subscribe(item => {
      this.item = item;
    })
  }

  addToCart(): void{
    this.cartService.addToCart(this.itemId);
    this.toasterService.success(this.item.name + ' added to cart');
  }
}
