import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SideNavData } from '../../models/misc/SideNavData.model';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  public sideNavData$ = new Subject<SideNavData>();

  constructor() { }

  public set sideNavData(value: SideNavData) {
    this.sideNavData$.next(value);
  }

}
