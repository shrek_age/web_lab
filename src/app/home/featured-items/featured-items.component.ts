import { Component, OnDestroy, OnInit } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { ItemModel } from 'src/app/models/misc/api/Item.model';
import { ItemsService } from 'src/app/services/items.service';

@AutoUnsubscribe()
@Component({
  selector: 'app-featured-items',
  templateUrl: './featured-items.component.html',
  styleUrls: ['./featured-items.component.sass']
})
export class FeaturedItemsComponent implements OnInit, OnDestroy {

  items = new Array<ItemModel>();

  constructor(private itemsService: ItemsService) { }
  ngOnDestroy(): void {
  }

  ngOnInit(): void {
    this.itemsService.getFeaturedItems().subscribe(im => this.items = im);
  }

}
