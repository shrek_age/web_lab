import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { SidenavService } from 'src/app/services/misc/sidenav.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  private sidenavOpen: boolean = false;

  constructor(private sidenavService: SidenavService) { }

  ngOnInit(): void {

  }

  toggleSideNav(){
    this.sidenavOpen = !this.sidenavOpen;
    this.sidenavService.sideNavData = {
      shouldBeOpen: this.sidenavOpen
    };
  }

}
