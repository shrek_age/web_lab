import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home/home.component';
import { ItemComponent } from './items/item/item.component';

const routes: Routes = [{
  path: '',
  component: HomeComponent,
},
{
  path: 'item/:itemId',
  component: ItemComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
