import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { MatControlsModule } from '../mat-controls.module';
import { AuthControlsModule } from '../auth-controls/auth-controls.module';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { CartComponent } from './cart/cart.component';
@NgModule({
  declarations: [
    NavbarComponent,
    SideMenuComponent,
    LayoutComponent,
    CartComponent,
  ],
  imports: [
    CommonModule,
    MatControlsModule,
    AuthControlsModule,
    AppRoutingModule
  ],
  exports: [
    NavbarComponent,
    SideMenuComponent,
    LayoutComponent,
  ]
})
export class SharedModule { }
