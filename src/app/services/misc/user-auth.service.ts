import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Observable } from 'rxjs';
import Auth0Lock from 'auth0-lock';
import jwtDecode from 'jwt-decode';
import { UserModel } from '../../models/misc/User.model';
import { auth0LockOptions, AuthConfig } from '../../shared/auth/auth-config';

const enum LocalStorage {
  IdToken = 'id_token',
  AccessToken = 'access_token',
  ExpiresAt = 'expires_at'
}

@Injectable({
  providedIn: 'root'
})
export class UserAuthService  {

  private userMetadata: UserModel = {
      imageUrl: '',
      fullName: '',

  };

  private lock = new Auth0Lock(AuthConfig.clientID, AuthConfig.domain, auth0LockOptions);

  private auth$ = new BehaviorSubject<boolean>(false);
  private user$ = new Subject<UserModel>();

  isAuthenticated$ = this.auth$.asObservable();
  userInfo$ = this.user$.asObservable();

  constructor() {
  }

  authenticate = () => {
    if (this.isAuthenticated()) {
      this.authenticateFromToken();
    } else {
      this.login();
    }

    this.lock.on('authenticated', (authResult: any) => {
      this.setSession(authResult);
      if (!authResult.idTokenPayload) {
        this.dispatchUserFromToken(jwtDecode(authResult.id_token));
      } else {
        this.dispatchUserFromToken(authResult.idTokenPayload);
      }

      this.getUserInfo();
    });

    this.lock.on('authorization_error', (error: any) => console.error(error));
  }

  login = () => this.lock.show();

  logout = () => {
    localStorage.removeItem(LocalStorage.IdToken);
    localStorage.removeItem(LocalStorage.AccessToken);
    localStorage.removeItem(LocalStorage.ExpiresAt);
    this.auth$.next(false);
  }

  getUserInfo(): void {
    this.lock.getUserInfo(localStorage.getItem(LocalStorage.AccessToken) || '', (error, profile) => {
      if (!error) {
        this.user$.next(
          {
            imageUrl: profile.picture || '',
            fullName: profile.name,
          });
      }
    });
  }

  getToken = () => localStorage.getItem(LocalStorage.IdToken);
  
  isAuthenticated(): boolean {
    let token = localStorage.getItem(LocalStorage.AccessToken) || 'null';
    return this.tokenExpired(token);
  } 

  get authenticated(): boolean {
    return this.isAuthenticated();
  }

  private authenticateFromToken = () => {
    const token = this.getToken();
    if (!token) {
      return;
    }
    this.dispatchUserFromToken(jwtDecode(token));
    this.getUserInfo();
  }

  private tokenExpired(token: string) {
    if(token === 'null'){
      return false;
    }
    const expiry = (JSON.parse(atob(token.split('.')[1]))).exp;
    return (Math.floor((new Date).getTime())) >= expiry;
  }

  private dispatchUserFromToken = (token: any) => {
    this.userMetadata.imageUrl = token.picture;
    this.userMetadata.fullName = token.name;
    this.user$.next(this.userMetadata);
    this.auth$.next(true);
  }

  private setSession = ({ expiresIn, idToken, accessToken }: auth0.Auth0DecodedHash) => {
    if (idToken) {
      localStorage.setItem(LocalStorage.IdToken, idToken);
    }
    if (accessToken) {
      localStorage.setItem(LocalStorage.AccessToken, accessToken);
    }
    if (expiresIn) {
      localStorage.setItem(LocalStorage.ExpiresAt, JSON.stringify(expiresIn * 1000 + new Date().getTime()));
    }
  }
}
