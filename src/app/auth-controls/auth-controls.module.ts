import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginButtonComponent } from './login-button/login-button.component';
import { MatControlsModule } from '../mat-controls.module';
import { LogoutButtonComponent } from './logout-button/logout-button.component';
import { AuthBarComponent } from './auth-bar/auth-bar.component';

@NgModule({
  declarations: [
    LoginButtonComponent,
    LogoutButtonComponent,
    AuthBarComponent
  ],
  imports: [
    CommonModule,
    MatControlsModule,
  ], 
  exports: [
    LoginButtonComponent,
    LogoutButtonComponent,
    AuthBarComponent,
  ]
})
export class AuthControlsModule { }
