import { environment } from 'src/environments/environment';

export const AuthConfig = {
  clientID: environment.auth0.clientId,
  domain: environment.auth0.domain,
  callbackURL: environment.auth0TokenConfig.callbackUrl,
  scope: environment.auth0TokenConfig.scope,
  audience: environment.auth0TokenConfig.audience
};

const params: Auth0LockAuthParamsOptions = {
  scope: AuthConfig.scope
};

const auth: Auth0LockAuthOptions = {
  responseType: 'token id_token',
  params: params,
  redirectUrl: AuthConfig.callbackURL,
  redirect: false,
  autoParseHash: true,
  sso: false,
  audience: AuthConfig.audience
};

export const auth0LockOptions: Auth0LockConstructorOptions = {
    auth: auth,
    socialButtonStyle: 'small',
    autoclose: true,
    rememberLastLogin: true,
    closable: true,
};
