import { Component, OnDestroy, OnInit } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { UserModel } from 'src/app/models/misc/User.model';
import { UserAuthService } from 'src/app/services/misc/user-auth.service';

@AutoUnsubscribe()
@Component({
  selector: 'app-auth-bar',
  templateUrl: './auth-bar.component.html',
  styleUrls: ['./auth-bar.component.sass']
})
export class AuthBarComponent implements OnInit, OnDestroy{
  isAuthenticated: boolean = false;
  userModel!: UserModel;

  constructor(public auth: UserAuthService) { }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {

    this.auth.userInfo$.subscribe((user: UserModel) => {
      this.isAuthenticated = true;
      this.userModel = user;
    });

    this.auth.isAuthenticated$.subscribe(v => {
      this.isAuthenticated = v;
    });

    if(this.auth.isAuthenticated()){
      this.auth.getUserInfo();
    }
  }
}
