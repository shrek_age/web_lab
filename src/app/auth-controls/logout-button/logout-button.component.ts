import { ThrowStmt } from '@angular/compiler';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UserAuthService } from 'src/app/services/misc/user-auth.service';

@Component({
  selector: 'app-logout-button',
  templateUrl: './logout-button.component.html',
  styleUrls: ['./logout-button.component.sass']
})
export class LogoutButtonComponent {

  constructor(private auth: UserAuthService) { }
  
  logout(): void{
    this.auth.logout();
  }

}
