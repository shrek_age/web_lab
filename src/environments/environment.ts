// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: "https://localhost:44480/api",
  auth0:{
    domain: "dev-5hjk51ci.us.auth0.com",
    clientId: "kGpm6SocxJ8NjUxojjWS6UgMXt8EdVdh",
  },
  auth0TokenConfig:{
    audience: "https://scms.com",
    scope: "openid profile email",
    callbackUrl: "http://localhost:4200"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
