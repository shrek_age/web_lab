import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { SidenavService } from 'src/app/services/misc/sidenav.service';

@AutoUnsubscribe()
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.sass']
})
export class LayoutComponent implements OnInit, OnDestroy {

  sideNavOpen: boolean = false;

  constructor(private sidenavService: SidenavService) { }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
    this.sidenavService.sideNavData$.subscribe(v => {
      this.sideNavOpen = v.shouldBeOpen;
    });
  }

}
