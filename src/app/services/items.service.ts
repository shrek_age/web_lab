import { Injectable } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { ItemModel } from '../models/misc/api/Item.model';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {
  private items: ItemModel[] = [{
    id: "1",
    name: "Item 1",
    price: 500.00,
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget erat vel neque lacinia molestie dignissim in orci. Duis placerat, libero pretium sollicitudin luctus, quam elit molestie enim, ac porta ligula nisi dignissim nulla. Vestibulum aliquam est ornare sem gravida gravida. Morbi sed tellus in justo vehicula rhoncus quis eu.",
    imgUrl: "",
    rating: 3, 
    remainingInStock: 10
  },
  {
    id: "2",
    name: "Item 2",
    price: 22.88,
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget erat vel neque lacinia molestie dignissim in orci. Duis placerat, libero pretium sollicitudin luctus, quam elit molestie enim, ac porta ligula nisi dignissim nulla. Vestibulum aliquam est ornare sem gravida gravida. Morbi sed tellus in justo vehicula rhoncus quis eu.",
    imgUrl: "",
    rating: 1, 
    remainingInStock: 10
  },
  {
    id: "3",
    name: "Item 3",
    price: 100.00,
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget erat vel neque lacinia molestie dignissim in orci. Duis placerat, libero pretium sollicitudin luctus, quam elit molestie enim, ac porta ligula nisi dignissim nulla. Vestibulum aliquam est ornare sem gravida gravida. Morbi sed tellus in justo vehicula rhoncus quis eu.",
    imgUrl: "",
    rating: 5, 
    remainingInStock: 10
  }
]

  constructor(private route: ActivatedRoute) { }

  public getFeaturedItems(): Observable<ItemModel[]>{
    return of(this.items);
  }

  public getCurrentItemId(): Observable<string>{
    return this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>{
        return params.get("itemId") || '';
      }
    ));
  }

  public getItemById(id: string): Observable<ItemModel | undefined>{
    return of(this.items.find(v => v.id === id));
  }
}
