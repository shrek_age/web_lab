import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemComponent } from './item/item.component';
import { MatControlsModule } from '../mat-controls.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
@NgModule({
  declarations: [
    ItemComponent
  ],
  imports: [
    CommonModule,
    MatControlsModule,
    NgbModule,
    ToastrModule.forRoot()
  ]
})
export class ItemsModule { }
