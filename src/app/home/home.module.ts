import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturedItemsComponent } from './featured-items/featured-items.component';
import { ItemCardComponent } from './item-card/item-card.component';
import { MatControlsModule } from '../mat-controls.module';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [
    FeaturedItemsComponent,
    HomeComponent,
    ItemCardComponent
  ],
  imports: [
    CommonModule,
    MatControlsModule,
    RouterModule, 
    NgbModule
  ],
  exports: [
    FeaturedItemsComponent
  ]
})
export class HomeModule { }
