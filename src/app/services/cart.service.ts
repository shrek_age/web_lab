import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  public cart$ = new Subject<string>();
  constructor() { }

  public addToCart(itemId: string): void{
    this.cart$.next(itemId);
  }
}
