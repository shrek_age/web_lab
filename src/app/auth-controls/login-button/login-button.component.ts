import { Component, OnDestroy, OnInit } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { UserAuthService } from 'src/app/services/misc/user-auth.service';

@AutoUnsubscribe()
@Component({
  selector: 'app-login-button',
  templateUrl: './login-button.component.html',
  styleUrls: ['./login-button.component.sass']
})
export class LoginButtonComponent  implements OnDestroy{

  constructor(private auth: UserAuthService) { }

  ngOnDestroy(): void {
  }

  login(){
    this.auth.authenticate();
  }

}
