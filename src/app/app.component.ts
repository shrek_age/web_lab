import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'scms';
  sideNavOpened: boolean = false;
  toggleSideNav(toggle: boolean){
    this.sideNavOpened = toggle;
  }

}
