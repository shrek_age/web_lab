import { Component, Input, OnInit } from '@angular/core';
import { ItemModel } from 'src/app/models/misc/api/Item.model';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.sass']
})
export class ItemCardComponent implements OnInit {

  @Input() item!: ItemModel;
  
  constructor() { }

  ngOnInit(): void {
  }
}
