export interface ItemModel{ 
    id: string;
    name: string;
    price: number;
    imgUrl: string;
    rating: number;
    description: string;
    remainingInStock: number;
}